﻿using System;
using System.Collections.Generic;

namespace LeanMVC
{
    public interface IEvent
    {
    }

    public abstract class Event : IEvent
    {
        private readonly HashSet<Action> _listener = new HashSet<Action>();

        public void AddListener(Action callback)
        {
            if (!_listener.Contains(callback))
                _listener.Add(callback);
        }

        public void RemoveListener(Action callback)
        {
            _listener.Remove(callback);
        }

        public void Raise()
        {
            foreach (var listener in _listener)
                listener.Invoke();
        }
    }

    public abstract class Event<T> : IEvent
    {
        private readonly HashSet<Action<T>> _listener = new HashSet<Action<T>>();

        public void AddListener(Action<T> callback)
        {
            if (!_listener.Contains(callback))
                _listener.Add(callback);
        }

        public void RemoveListener(Action<T> callback)
        {
            _listener.Remove(callback);
        }

        public void Raise(T param)
        {
            foreach (var listener in _listener)
                listener.Invoke(param);
        }
    }

    public abstract class Event<T1, T2> : IEvent
    {
        private readonly HashSet<Action<T1, T2>> _listener = new HashSet<Action<T1, T2>>();

        public void AddListener(Action<T1, T2> callback)
        {
            if (!_listener.Contains(callback))
                _listener.Add(callback);
        }

        public void RemoveListener(Action<T1, T2> callback)
        {
            _listener.Remove(callback);
        }

        public void Raise(T1 param1, T2 param2)
        {
            foreach (var listener in _listener)
                listener.Invoke(param1, param2);
        }
    }

    public abstract class Event<T1, T2, T3> : IEvent
    {
        private readonly HashSet<Action<T1, T2, T3>> _listener = new HashSet<Action<T1, T2, T3>>();

        public void AddListener(Action<T1, T2, T3> callback)
        {
            if (!_listener.Contains(callback))
                _listener.Add(callback);
        }

        public void RemoveListener(Action<T1, T2, T3> callback)
        {
            _listener.Remove(callback);
        }

        public void Raise(T1 param1, T2 param2, T3 param3)
        {
            foreach (var listener in _listener)
                listener.Invoke(param1, param2, param3);
        }
    }

    public abstract class Event<T1, T2, T3, T4> : IEvent
    {
        private readonly HashSet<Action<T1, T2, T3, T4>> _listener = new HashSet<Action<T1, T2, T3, T4>>();

        public void AddListener(Action<T1, T2, T3, T4> callback)
        {
            if (!_listener.Contains(callback))
                _listener.Add(callback);
        }

        public void RemoveListener(Action<T1, T2, T3, T4> callback)
        {
            _listener.Remove(callback);
        }

        public void Raise(T1 param1, T2 param2, T3 param3, T4 param4)
        {
            foreach (var listener in _listener)
                listener.Invoke(param1, param2, param3, param4);
        }
    }
}