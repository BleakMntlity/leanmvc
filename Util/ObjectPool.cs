using System.Collections.Generic;

namespace LeanMVC
{

	public class ObjectPool<T> where T : class, new()
	{
		private Stack<T> m_stack;

		public ObjectPool(int _size)
		{
			m_stack = new Stack<T>(_size);
		}

		public T GetObject()
		{
			if (m_stack.Count != 0)
				return m_stack.Pop();

			return new T();
		}

		public void Return(T _object)
		{
			m_stack.Push(_object);
		}
	}
}