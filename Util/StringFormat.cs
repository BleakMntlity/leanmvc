﻿using System.Text;

namespace LeanMVC.Util
{
	public static class StringFormat
	{
		private static StringBuilder m_stringBuilder = new StringBuilder();
		public static string Concatenate(params string[] _params)
		{
			for (var i = 0; i < _params.Length; ++i)
			{
				m_stringBuilder.Append(_params[i]);
			}	
			var str = m_stringBuilder.ToString();
			m_stringBuilder.Length = 0;
			return str;
		}
	}
}

