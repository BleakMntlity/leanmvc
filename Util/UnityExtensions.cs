﻿using UnityEngine;

namespace LeanMVC.Util
{
	public static class UnityExtensions
	{
		public static void ReparentAndReset(this Transform _self, Transform _parent)
		{
			_self.SetParent(_parent);
			_self.localScale = Vector3.one;
			_self.localPosition = Vector3.zero;
			_self.localRotation = Quaternion.identity;
		}

		public static void ReparentAndResetNet(this NetworkedView _self, Transform _parent)
		{
			_self.StartParent = _parent;
			_self.StartLocalPosition = Vector3.zero;
			_self.transform.localScale = Vector3.one;
			_self.StartLocalRotation = Quaternion.identity;
		}
	}
}

