﻿using UnityEngine;

namespace LeanMVC
{
	public static class MathHelper
	{

		public static readonly Vector2 Invalid2f = new Vector2(float.NaN, float.NaN);
		public static readonly Vector3 Invalid3f = new Vector3(float.NaN, float.NaN, float.NaN);

		public static bool IsInvalid(this Vector2 vector2)
		{
			return vector2 == Invalid2f;
		}

		public static bool IsInvalid(this Vector3 vector3)
		{
			return vector3 == Invalid3f;
		}

		public static float DotProduct(Vector3 first, Vector3 second)
		{
			return (first.x*second.x) + (first.y*second.y) + (first.z*second.z);
		}

		private static Vector3 GetRayPlaneIntersection(Ray ray, Vector3 planeNormal, Vector3 planePoint)
		{
			float denom = DotProduct(planeNormal, ray.direction);
			if (denom <= 0.0001f)
				//plane and ray are parallel and will never intersect (or always)
				return Invalid3f;

			//Distance at which they intersect
			var t = DotProduct(planePoint - ray.origin, planeNormal)/denom;
			//The point at which they intersect
			return ray.origin + ray.direction*t;
		}

		private static bool IsInsideCone(Vector3 point, CircularCone cone)
		{
			var dist = DotProduct(point - cone.Tip, cone.Direction);
			if (dist <= 0 || dist >= cone.Height)
				return false;

			var radius = (dist/cone.Height)*cone.BaseRadius;
			var orthDistance = ((point - cone.Tip) - dist*cone.Direction).magnitude;
			return orthDistance <= radius;
		}
	}


	public struct Signum
	{
		public int Sign;

		public Signum(float value)
		{
			if (value < -0.5f)
				Sign = -1;
			else if (value > 0.5f)
				Sign = 1;
			else
				Sign = 0;
		}
	}

	public struct CircularCone
	{
		public Vector3 Tip;
		public float Height;
		public Vector3 Direction;
		public float BaseRadius;
	}
}