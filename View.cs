﻿using UnityEngine;

namespace LeanMVC
{
    public class View : MonoBehaviour, IMvcComponent
    {
        public Context Context { get; set; }

        public virtual void PostContruct()
        {
        }

        protected virtual void Awake()
        {
            SingletonProvider.RegisterViewAtContext(this);
        }

        protected virtual void Start()
        {
        }

        protected virtual void OnEnable()
        {
        }

        protected virtual void OnDisable()
        {
        }

        protected virtual void OnDestroy()
        {
            Context.RemoveView(this);
        }
    }
}