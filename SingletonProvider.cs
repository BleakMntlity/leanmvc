﻿using System.Collections.Generic;

namespace LeanMVC
{
    public static class SingletonProvider
    {
        public static Context CurrentContext { get; private set; }
        private static readonly HashSet<IMvcComponent> BufferedViews = new HashSet<IMvcComponent>();

        public static void SetCurrentContext(Context newContext)
        {
            if (CurrentContext != null)
                CurrentContext.Reset();

            CurrentContext = newContext;
            foreach (var view in BufferedViews)
                CurrentContext.AddView(view);

            BufferedViews.Clear();
            CurrentContext.SetupBindings();
        }

        public static void RegisterViewAtContext(IMvcComponent view)
        {
            if (CurrentContext == null)
                BufferedViews.Add(view);
            else
                CurrentContext.AddView(view);
        }
    }
}