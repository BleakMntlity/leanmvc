﻿namespace LeanMVC
{
    public interface IMvcSingleton
    {
        Context Context { get; set; }
    }
}