﻿using System;
using System.Collections.Generic;

namespace LeanMVC
{
    public abstract class Context
    {
        protected readonly Dictionary<Type, IEvent> Events;
        protected readonly Dictionary<Type, ICommand> Commands;
        protected readonly Dictionary<Type, IMvcSingleton> Singletons;
        protected readonly Dictionary<Type, Type> Components;
        
        protected readonly HashSet<IMvcComponent> Views;

        protected Context()
        {
            Events = new Dictionary<Type, IEvent>();
            Commands = new Dictionary<Type, ICommand>();
            Singletons = new Dictionary<Type, IMvcSingleton>();

            Components = new Dictionary<Type, Type>();
            Views = new HashSet<IMvcComponent>();
        }

        public virtual void Reset()
        {
            Views.Clear();
            Events.Clear();
            Commands.Clear();
            Components.Clear();
        }

        public abstract void SetupBindings();

        public void AddView(IMvcComponent view)
        {
            view.Context = this;
            Views.Add(view);
        }

        public void RemoveView(IMvcComponent view)
        {
            Views.Remove(view);
        }

        public void AddEvent(IEvent @event)
        {
            Type type = @event.GetType();
            Events.Add(type, @event);
        }
        
        public T GetEvent<T>() where T : IEvent
        {
            IEvent @event;
            if (Events.TryGetValue(typeof(T), out @event))
                return (T) @event;
            return default(T);
        }

        public void SetCommand(ICommand command)
        {
            command.Context = this;
            command.Bind();
            Type type = command.GetType();
            Commands[type] = command;
        }

        public void RemoveCommand(ICommand command)
        {
            command.Unbind();
            Commands.Remove(command.GetType());
        }

        public void SetSingleton<T>(T singleton) where T :IMvcSingleton
        {
            singleton.Context = this;
            Singletons[typeof(T)] = singleton;
        }

        public T GetSingleton<T>() where T : IMvcSingleton
        {
            IMvcSingleton service;
            Singletons.TryGetValue(typeof(T), out service);
            return (T) service;
        }

        public void BindComponent<T, TU>() where TU : T, new() where T : IMvcComponent
        {
            Components[typeof(T)] = typeof(TU);
        }

        public T GetNewComponent<T>() where T : IMvcComponent, new()
        {
            var instance = new T();
            instance.Context = this;
            instance.PostContruct();
            return instance;
        }
    }
}