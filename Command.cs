﻿namespace LeanMVC
{
    public interface ICommand : IMvcSingleton
    {
        IEvent Binding { get; }
        void Bind();
        void Unbind();
    }

    public abstract class Command : ICommand
    {
        public Context Context { get; set; }
        public IEvent Binding { get { return _binding; } }
        
        private readonly Event _binding;
        
        protected Command(Event eventBinding)
        {
            _binding = eventBinding;
        }

        public abstract void Execute();
        
        public void Bind()
        {
            _binding.AddListener(Execute);
        }

        public void Unbind()
        {
            _binding.RemoveListener(Execute);
        }
    }

    public abstract class Command<T> : ICommand
    {
        public Context Context { get; set; }
        public IEvent Binding { get { return _binding; } }

        private readonly Event<T> _binding;
        
        protected Command(Event<T> eventBinding)
        {
            _binding = eventBinding;
        }

        public abstract void Execute(T param);
        
        public void Bind()
        {
            _binding.AddListener(Execute);
        }

        public void Unbind()
        {
            _binding.RemoveListener(Execute);
        }
    }
    
    public abstract class Command<T, TU> : ICommand
    {
        public Context Context { get; set; }
        public IEvent Binding { get { return _binding; } }
        
        private readonly Event<T, TU> _binding;

        protected Command(Event<T, TU> eventBinding)
        {
            _binding = eventBinding;
        }

        public abstract void Execute(T t, TU tu);
        
        public void Bind()
        {
            _binding.AddListener(Execute);
        }

        public void Unbind()
        {
            _binding.RemoveListener(Execute);
        }
    }
    
    public abstract class Command<T,TU, TV> : ICommand
    {
        public Context Context { get; set; }
        public IEvent Binding {get { return _binding; }}
        
        
        private readonly Event<T, TU, TV> _binding;

        protected Command(Event<T, TU, TV> eventBinding)
        {
            _binding = eventBinding;
        }

        public abstract void Execute(T t, TU tu, TV tv);
        
        public void Bind()
        {
            _binding.AddListener(Execute);
        }

        public void Unbind()
        {
            _binding.RemoveListener(Execute);
        }
    }
}