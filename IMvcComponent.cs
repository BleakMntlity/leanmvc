﻿namespace LeanMVC
{
	public interface IMvcComponent
	{
		Context Context { get; set; }
		void PostContruct();
	}
}	