﻿using UnityEngine;
using UnityEngine.Networking;

namespace LeanMVC
{
	public class NetworkedView : NetworkBehaviour, IMvcComponent
	{
		public Context Context { get; set; }
		
		[SyncVar] [HideInInspector] public Transform StartParent;
		[SyncVar] [HideInInspector] public Quaternion StartLocalRotation;
		[SyncVar] [HideInInspector] public Vector3 StartLocalPosition;

		public virtual void PostContruct()
		{
		}

		private void Awake()
		{
			SingletonProvider.RegisterViewAtContext(this);
			OnAwake();
		}

		protected virtual void OnAwake()
		{
		}

		private void Start()
		{
			if (StartParent != null)
			{
				gameObject.transform.SetParent(StartParent, false);
				gameObject.transform.localRotation = StartLocalRotation;
				gameObject.transform.localPosition = StartLocalPosition;
			}
			OnStart();
		}

		protected virtual void OnStart()
		{
		}

		private void OnEnable()
		{
			OnEnabled();
		}

		protected virtual void OnEnabled()
		{
		}

		private void Update()
		{
			OnUpdate();
		}

		protected virtual void OnUpdate()
		{
		}

		private void LateUpdate()
		{
			OnLateUpdate();
		}

		protected virtual void OnLateUpdate()
		{
		}

		private void OnTriggerEnter(Collider _collider)
		{
			OnTriggerEntered(_collider);
		}

		protected virtual void OnTriggerEntered(Collider _collider)
		{
		}

		private void OnDisable()
		{
			OnDisabled();
		}

		protected virtual void OnDisabled()
		{
		}

		private void OnDestroy()
		{
			OnDestroyed();
			Context.RemoveView(this);
		}

		protected virtual void OnDestroyed()
		{
		}
	}
}